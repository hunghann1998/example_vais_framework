//
//  GenKeyUserViewController.swift
//  UsingMyFramework
//
//  Created by Hưng Vũ on 08/09/2021.
//  Copyright © 2021 Nazario Mariano. All rights reserved.
//

import Foundation
import UIKit
import VaisFramework


class GenKeyUserViewController: UIViewController {


    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var labelKey: UILabel!
    @IBOutlet weak var btnGenKey: UIButton!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var lstFileAudio: UITableView!
    private var pathAudioGenKey = [String]()
    var startRecord = false;
    var data: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnRecord.isHidden = false
        btnVerify.isHidden = true
        labelKey.isHidden = true
        btnGenKey.isHidden = true
        lstFileAudio.delegate = self
        lstFileAudio.dataSource = self
//        view.backgroundColor = .link
        // Do any additional setup after loading the view.
    }
    @IBAction func genKey(_ sender: Any) {
        VaisSpeech.shared.genKey(listFile: pathAudioGenKey) { isGenKeyDone, error, data in
            if(!isGenKeyDone){
                let alert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                print("1231243432")
                self.pathAudioGenKey.removeAll()
                self.lstFileAudio.reloadData()
                self.btnGenKey.isHidden = true
                self.btnRecord.isHidden = true
                self.btnVerify.isHidden = false
                self.labelKey.isHidden = false
                
                self.data = data
                
                
            }
        }
    }

    @IBAction func verify(_ sender: Any) {
        let vc = (storyboard?.instantiateViewController(identifier: "verify"))! as VerifyViewController
        vc.data = data
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func record(_ sender: Any) {
        if(!startRecord){
            VaisSpeech.shared.startRecording(callback:{ result,mess in
                if(result){
                    self.btnRecord.setTitle("STOP", for: .normal)
                    self.startRecord = true;
                }else{
                    let alert = UIAlertController(title: "Error", message: mess, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }

            })
           
        }else{
            VaisSpeech.shared.stopRecording(callback:{ result,mess in
                if(result){
                    self.btnRecord.setTitle("START", for: .normal)
                    self.startRecord = false;
                    self.btnGenKey.isHidden = false
                    pathAudioGenKey.append(mess)
                    lstFileAudio.beginUpdates()
                    lstFileAudio.insertRows(at: [IndexPath(row: pathAudioGenKey.count-1, section: 0)], with: .automatic)
                    lstFileAudio.endUpdates()
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: mess, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
            btnRecord.setTitle("START", for: .normal)
            startRecord = false;
           
        }

        
        
    }
}

extension GenKeyUserViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("tap")
    }
}

extension GenKeyUserViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        pathAudioGenKey.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath)
        cell.textLabel?.text  = pathAudioGenKey[indexPath.row]
        return cell
    }
    
    
}

