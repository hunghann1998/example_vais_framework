//
//  VerifyViewController.swift
//  UsingMyFramework
//
//  Created by Hưng Vũ on 13/08/2021.
//  Copyright © 2021 Nazario Mariano. All rights reserved.
//

import UIKit
import VaisFramework


class VerifyViewController: UIViewController {
    
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var txtKey: UILabel!
    public var data: Data?
    @IBOutlet weak var btnRecord: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if(data != nil){
            txtKey.text = "xxxxxxxxxxxx-xxxxxxxxx-xxxxxxxxxxxxxxx"
        }else{
            txtKey.text = "null"
        }
       
        btnRecord.setTitle("START", for: .normal)
        btnVerify.isHidden = true
        // Do any additional setup after loading the view.
    }

    var startRecord = false;

    
    @IBAction func record(_ sender: Any) {
        if(data != nil){
            if(!startRecord){
                VaisSpeech.shared.startRecording(callback:{ result,mess in
                    if(result){
                        self.btnRecord.setTitle("STOP", for: .normal)
                        self.startRecord = true;
                    }else{
                        let alert = UIAlertController(title: "Error", message: mess, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }

                })
               
            }else{
                VaisSpeech.shared.stopRecording(callback:{ result,mess in
                    if(result){
                        self.btnRecord.setTitle("START", for: .normal)
                        self.startRecord = false;
                        VaisSpeech.shared.verify(file: mess, data: self.data!) { isVerify, mess in
                            print("fsagidpogdfg")
                            print(isVerify)
                            if(isVerify){
                                let alert = UIAlertController(title: "Error", message: "That's you", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                let alert = UIAlertController(title: "Error", message: mess ?? "Not you", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else{
                        let alert = UIAlertController(title: "Error", message: mess, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                })
                btnRecord.setTitle("START", for: .normal)
                startRecord = false;
            }
        }else{
            let alert = UIAlertController(title: "Error", message: "key null", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }
   
    
    @IBAction func verify(_ sender: Any) {
        print("23423423423432")

    }
    
}
        

